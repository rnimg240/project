class Product < ActiveRecord::Base
	belongs_to :product_type
	has_many :likes, dependent: :nullify
	has_many :comments, dependent: :nullify	

	validates :name, presence: true
end
