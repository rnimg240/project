class LikesController < ApplicationController
	before_action :lookup_project

	def create
    @like = Like.new
    @like.product = @product
    @like.save
      respond_to do |format|
      format.html { redirect_to root_url }
      format.js {}
    end  
  end

  def lookup_project
 		@product = Product.find(params[:product_id])
  end	

end
