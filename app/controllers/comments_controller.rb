class CommentsController < ApplicationController
	before_action :lookup_project

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
    format.html {redirect_to @comment}
    format.js {} #automatically call destroy.js.erb , because it calls by default the name of the method
    end 
  end

  def create 
  	@comment = @product.comments.new(comment_params)
  	@comment.save    
    respond_to do |format|
      format.html { redirect_to @comment}
      format.js {}
    end
  end	

  def lookup_project
  	@product = Product.find(params[:product_id])
  end	

  def comment_params
  	params.require(:comment).permit(:body,:product_id)
  end	


end
