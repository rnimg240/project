Product.destroy_all

x = ProductType.create!(name: "Lotion")
y = ProductType.create!(name: "Perfume")

# Create the Perfumes
Product.create!(product_type_id: y.id, name: "Sintonia", price: "101.00", description: "The Natura Tuning Deodorant Cologne Male has woody fragrance and engaging.")
Product.create!(product_type_id: y.id, name: "Homem", price: "105.00", description: "For man to live , feel and make his mark throughout his journey. Wood, refreshing, ginger.")
Product.create!(product_type_id: y.id, name: "Amo", price: "93.20", description: "Amó Heats AND A unusual spices Combination with hum sultry chord notes of cognac and Woods . A special toast Who Moves love , awakening the senses and leaving STILL Warmest the Celebration The Two")

# Create the Lotions
Product.create!(product_type_id: x.id, name: "Sou", price: "11.90", description: "Without dye , fragrance-free")
Product.create!(product_type_id: x.id, name: "Aconchego", price: "24.80", description: "Its comfortable fragrance is inspired by the combination of lavender and vanilla.")
Product.create!(product_type_id: x.id, name: "Ekos", price: "18.90", description: "Enriched with acai oil.")

